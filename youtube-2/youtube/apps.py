import urllib.request

from django.apps import AppConfig
from .ytchannel import YTChannel
from .models import Video

class YouTubeConfig(AppConfig):
    name = 'youtube'

    def ready(self):
        global videos

        url = 'https://www.youtube.com/feeds/videos.xml?channel_id=' \
            + 'UC300utwSVAYOoRLEqmsprfg'
        xmlStream = urllib.request.urlopen(url)
        channel = YTChannel(xmlStream)
        for video in channel.videos():
            list = Video.(titulo = video.title, url = video.link, selected = video.select, id = video.id)
            list.save()
